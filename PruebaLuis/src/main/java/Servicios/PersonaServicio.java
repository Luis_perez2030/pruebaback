package Servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import Entidad.personasLuis;
import Interface.PersonaInterface;

public class PersonaServicio implements PersonaInterface  {

	@Autowired
	private Repositorio.PersonaRepositorio Repositorio;
	
	@Override
	public personasLuis Crear(personasLuis Input) {
		return Repositorio.save(Input);
	}

	@Override
	public personasLuis Editar(personasLuis Input) {
		return Repositorio.save(Input);
	}

	@Override
	public Void Eliminar(int Id) {
		return Repositorio.delete(Id);
	}

	@Override
	public List<personasLuis> listar() {		
		return Repositorio.findAll();
	}

	@Override
	public personasLuis ListarPorId(int Id) {
		return Repositorio.findOne(Id);
	}

}
