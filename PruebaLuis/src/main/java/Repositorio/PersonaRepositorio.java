package Repositorio;

import java.util.List;

import org.springframework.data.repository.Repository;

import Entidad.personasLuis;


public interface PersonaRepositorio extends Repository<personasLuis, Integer> {
	
	personasLuis save(personasLuis Per);
	Void delete(int Id);
	List<personasLuis>findAll();
	personasLuis findOne(int Id);	
}
