package Controlador;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Entidad.personasLuis;
import Servicios.PersonaServicio;

@RestController
@RequestMapping(value="/prueba/v1/")
public class PersonaController {
	
	@Autowired
	PersonaServicio Service;

	//CREAR PERSONA
	@PostMapping(value="/save")
	public ResponseEntity<personasLuis>Crear(@RequestBody personasLuis Input){
		try {
			personasLuis per=Service.Crear(Input);
			return new ResponseEntity<personasLuis>(per,HttpStatus.OK);
		}
		catch(Exception ex) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	//ACTUALIZAR PERSONA
	@PutMapping(value="/update")
	public ResponseEntity<personasLuis>Actualizar(@RequestBody personasLuis Input){
		try {
			personasLuis per=Service.Crear(Input);
			return new ResponseEntity<personasLuis>(per,HttpStatus.OK);
		}
		catch(Exception ex) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
	}
	//ELIMINAR PERSONA
	@DeleteMapping(value="/delete/{id}")
	public ResponseEntity<personasLuis>Eliminar(@PathVariable int Id){
		personasLuis per=Service.ListarPorId(Id);
		if(per!=null) {
			Service.Eliminar(Id);
		}else {
			return new ResponseEntity<personasLuis>(per,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<personasLuis>(per,HttpStatus.OK);
	}
	//OBTENER PERSONAS
	@GetMapping(value="/all")
	public List<personasLuis>ObtenerTodos(){
		return Service.listar();
		
	}
	//OBTENER PERSONAS POR ID
	@GetMapping(value="/find/{id}")
	public ResponseEntity<personasLuis>ObtenerPersonaPorId(@PathVariable int Id){
		try {
			personasLuis per=Service.ListarPorId(Id);
			return new ResponseEntity<personasLuis>(per,HttpStatus.OK);
		}
		catch(Exception ex) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
}
